# Jenkins Pipeline for Node.js Application

This Jenkins Pipeline script is designed to automate the build process of a Node.js application. The application is containerized using Docker and pushed to a Docker registry.

## Stages of Jenkins Pipeline

1. **Checkout Code**: This step will clone the main branch of git repo to the agent node
2. **Install Dependencies**: This step will install all the dependencies that are required for the node.js application. This also involes a change directory step as package.json is present inside node-hello-world directory.
3. **Build docker image**: This step will create docker image of node-hello-world application based on the Dockerfile present in the root Directory of repo. 
4. **Push**: This step furthur contains two sub steps. First step is to login to Dockerhub using credentials that are stored in jenkins credentials. Once it is successfully logged in it will push the image to DockerHub.

## Setup Details

1. Using Docker create a master jenkins container. Access the Jenkins UI through a web browser and complete the initial setup. 
2. Install nodejs and Docker-tools plugins.
3. The agent machine should have docker installed.
4. Configure nodejs and docker tools, as they will be required in the pipeline.
5. Add docker hub creds in jenkins credentials and add appropriate id to it. Use that id inside the pipeline.
6. Using UI create a new pipeline and add this repo link and JenkinsFile as the path of jenkinsfile in the configuration.
7. This will create a pipeline that can be manually triggered, the next section will explain how to automate this step.


## Trigger Pipeline on each commit
1. Using gitlab create a personal access token.
2. In jenkins, install gitlab plugin.
3. Select Manage Jenkins -> Configure System, in that in the GitLab section, select Enable authentication for ‘/project’ end-point. Enter your access token there.
4. Now using jenkins UI setup the job/pipeline.
5. Add a webhook in GitLab settings, pointing to Jenkins.
6. Once all this is done pipeline will be triggered after each push.


# GitLab CI/CD for tcp-echo-server App

The .gitlab-ci.yml script is used build the tcp-echo-server. Apart from that the application is containerized using Docker and pushed to a Docker registry.  

## Stages of Gitlab CI

1. **build-app**: This step uses maven image to create a jar file from provided pom.xml. This file is transferred to the next job using artifacts. This artifact is required because gitlab always removes the additional files while moving on the next job. Using this we can define that do not clean this file, it is required in the subsequent steps. The application is build using mvn clean package command.
2. **create-docker-image**: This step contains a before_script section which uses variables created in gitlab(setting->CI/CD->variables) to login to docker hub. After logging it creates docker image of node-hello-world application and upload it to dockerhub. The artifact defined in the previous job can be used using a special parameter called dependencies, where job name whose artifacts to be used should added and it will provide you all the artifacts that can be used in this job. 

## Setup a Gitlab runner

1. Install gitlab-runner binary on local-machine/server. I have installed it locally using brew.
2. Using gitlab ui create new runner, add specific tags or allow untagged jobs. For this assignment i have allowed untagged jobs in my gitlab runner.
3. Added all other basic configs like runner-name etc.
4. In your local machine/server run gitlab-runner register, and add basic details regarding runner.
5. To start the runner run "gitlab-runner run" command.
6. This will start the runner, attach your project to this runner using gitlab UI.
7. Once all of this is done ci pipeline will run automatically every time a push is made.

### Challenges Faced
While writing Gitlab CI file i got stuck on how to pass jar file from one step to another.
### Solutions applied
Used artifact and depencies to avoid cleaning up of the jar file.
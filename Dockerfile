FROM node:18
COPY ./node-hello-world .
WORKDIR /node-hello-world
RUN npm install 
ENTRYPOINT npm start


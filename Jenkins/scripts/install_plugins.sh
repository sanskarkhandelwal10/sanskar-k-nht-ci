#!/bin/bash

echo "Downloading jenkins-cli.jar"

wget http://localhost:9090/jnlpJars/jenkins-cli.jar 

echo "Press Enter to start updating plugins... "

read -n 1

for i in $(cat plugins.txt); 
do
arrIN=(${i//:/ }) 
java -jar jenkins-cli.jar -s http://localhost:9090/ -webSocket -auth @./creds install-plugin https://updates.jenkins.io/download/plugins/${arrIN[0]}/${arrIN[1]}/${arrIN[0]}.hpi ;  
done
java -jar jenkins-cli.jar -s http://localhost:9090/ -webSocket -auth @./creds safe-restart;

echo "Plugins installed!"

sudo rm jenkins-cli.jar 2>/dev/null